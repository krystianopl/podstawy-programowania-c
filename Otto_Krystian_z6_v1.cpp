#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

struct Student
{
  char ini[2];
  double ocena;
};


int main()
{
  const int w=5, k=6, G=5;
  Student A[w][k];

  srand (time(0));

  cout << "Tablica rekordow:"<< endl;
  for (int i = 0; i < w; i++){ 
    for (int j = 0; j < k; j++) {
      A[i][j].ini[0] ='A' + rand( )%('Z'+1-'A'); 
      A[i][j].ini[1] ='A' + rand( )%('Z'+1-'A'); 
      A[i][j].ocena = G*rand( )/double(RAND_MAX);
      cout << fixed << setprecision(2) << setw(6) << A[i][j].ini[0] << A[i][j].ini[1] << " " << A[i][j].ocena;
    }
  cout << endl;
  }

  Student tmp; 
  for (int i = 0; i < w; i++)  
    if (A[i][0].ini[0]=='A' || A[i][0].ini[0]=='B' ){  
      tmp=A[i][0];
      for (int j = 0; j < k - 1; j++) 
        A[i][j] = A[i][j + 1]; 
      A[i][k - 1]= tmp; 
    }

  cout << "Przesunieta tablica:"<< endl;
  for (int i = 0; i < w; i++){ 
    for (int j = 0; j < k; j++) 
      cout << fixed << setprecision(2) << setw(6) << A[i][j].ini[0] << A[i][j].ini[1] << " " << A[i][j].ocena;
  cout << endl;
  }
  return 0;
}
