#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <fstream>

using namespace std;

int main()
{
  const int n = 4,X = 11, M=2,D=6;
  string nazwa;
  double A[n][n],najw=-99999,najm=99999;
  int najw_indeks=0; 

  cout << "Podaj nazwe pliku:\n"; 
  getline (cin, nazwa); 

  ifstream plk(nazwa); 
  if ( !plk.is_open() ) {
  cout << " Nie ma takiego pliku " << endl ; 
  return 1;}

  for (int i = 0; i < n; i++) 
    for (int j = 0; j < n; j++) 
      A[i][j] = X;  
  for (int i = 0; i < n; i++) 
    for (int j = 0; j < n; j++) 
      plk >> A[i][j];

  cout << "Wczytana tablica:"<< endl;
  for (int i = 0; i < n; i++) { 
    for (int j = 0; j < n; j++){ 
      cout << fixed << setprecision(M) << setw(D)<< A[i][j];
      if (i==j && A[i][j]>najw){
        najw=A[i][j];
        najw_indeks=i;}
      if (j==(n-1-i) && A[i][j]<najm)
        najm=A[i][j];
      }
    cout << endl; }

  for (int i = 0; i < n; i++) { 
    for (int j = 0; j < n; j++){ 
      if (i==najw_indeks || j==najw_indeks)
        A[i][j]=najw;
    }
  }

  A[najw_indeks][najw_indeks]=najm;

  cout << "Tablica po zamianie"<< endl;
  for (int i = 0; i < n; i++) { 
    for (int j = 0; j < n; j++) 
      cout << fixed << setprecision(M) << setw(D)<< A[i][j]; 
    cout << endl; }

  ofstream wy("coDrugi.txt");

  for (int i = 0; i < n; i++) { 
    for (int j = 0; j < n; j++){
      if (i % 2 == 1)
        wy << fixed << setprecision(M) << setw(D)<< A[i][j]; 
    }
    if (i % 2 == 1)
      wy << endl;    
  }

  wy.close(); 
  plk.close(); 
  cout << "Plik wynikowy zostal utworzony\n";

  return 0;
}
