#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

int main()
{
  const int D=5;
  int current_number,previous_number,highest_negative=0,count_higher=0,num_dd_negative=0,count_overall=0;
  float avg_dd_negative,sum_dd_negative=0;

  do
  {
    cout << "Prosze podac liczbe calkowita: " << endl;
    cin >>current_number;

    if(current_number<0)
    {
      if (highest_negative==0)
        highest_negative=current_number;
      else if(current_number>highest_negative)
        highest_negative=current_number;
      
      if (current_number>=(-99) && current_number<=(-10)){
        num_dd_negative++;
        sum_dd_negative+=current_number;
        avg_dd_negative=sum_dd_negative/num_dd_negative;
      }
    }

    if (count_overall!=0){
      if (current_number>previous_number)
        count_higher++;
    }

    count_overall++;
    previous_number=current_number;
  }while(! (current_number%2==0 && current_number%D==0));

  if (highest_negative!=0) 
    cout << "Najwieksza liczba ujemna to: "<< highest_negative << endl;
  else
    cout << "Nie wczytano zadnej liczby ujemnej" << endl;

  if (num_dd_negative!=0)
    cout << "Srednia liczb dwucyfrowych ujemnych to: "<< avg_dd_negative << endl;
  else
    cout << "Liczba dwucyfrowa ujemna nie wystapila" << endl;
  
  cout << "Tyle razy wystapila sytuacja, gdy wczytana liczba byla wieksza od swojego poprzednika: "<< count_higher << endl;

  return 0;
}
