#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

int main()
{
  const int D=2, G=6, N=3;
  int k, capital_caption=0,bigger_than_avg=0;
  string caption;
  double number,avg,sum=0;
  double a[N];

  do
  {
    cout << "Prosze podac liczbe calkowita zawarta pomiedzy: "<< D << " a " << G << ": " << endl;
    cin >>k;
  }while(! (k>=D && k<=G));

  cout << "Zaakceptowano k wynoszace: " << k << endl;
  
  for (int i=0; i< k; i++) {
    cout << "Prosze wprowadzic napis:  " << endl;
    cin >> caption;
    if (caption[0]>= 'A' && caption[0] <= 'Z') capital_caption++;

    cout << "Zostalo do wprowadzenia: "<< k-i-1 << " napisow"<< endl;
  }

  cout << "Liczba wpisanych napisow zaczynajaca sie z duzej litery: " << capital_caption << endl;

  for (int i=0; i<N;i++){
    cout << "Prosze wprowadzic liczbe rzeczywista:  " << endl;
    cin>>a[i];
    sum+=a[i];
    cout << "Zostalo do wprowadzenia: "<< N-i-1 << " liczb rzeczywistych"<< endl;
  }

  avg=sum/N;
  cout << "Srednia wprowadzonych cyfr wynosi: " << avg << endl;

  for (int i=0; i<N; i++)
    if (a[i]>avg) bigger_than_avg++;
      
  cout << "Ilosc liczb znajdujacej sie powyzej sredniej : " << bigger_than_avg << endl;

  return 0;
}
