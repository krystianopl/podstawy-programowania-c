#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

int main()
{
  string user_name;
  int x_a,x_b,x_c,y_a,y_b,y_c;
  double bok_a_b,bok_b_c,bok_c_a,obwod,pole;
  double srodek_ciezkosci_x,srodek_ciezkosci_y,promien_okregu_opisanego,promien_okregu_wpisanego;
  bool warunek1,warunek2,warunek3;

  cout << "Dzien dobry!"<< endl << "Prosze sie przedstawic i podac swoje imie:"<< endl;
  cin >> user_name;
  cout << "Witaj, "<< user_name << "!" << endl;
  cout << "Bede teraz prosic o podanie wspolrzednych x,y wierzcholkow trojkata ABC"<< endl;
  cout << "Prosze podac wspolrzedna x punktu A"<< endl;
  cin >> x_a;
  cout << "Prosze podac wspolrzedna y punktu A"<< endl;
  cin >> y_a;
  cout << "Prosze podac wspolrzedna x punktu B"<< endl;
  cin >> x_b;
  cout << "Prosze podac wspolrzedna y punktu B"<< endl;
  cin >> y_b;
  cout << "Prosze podac wspolrzedna x punktu C"<< endl;
  cin >> x_c;
  cout << "Prosze podac wspolrzedna y punktu C"<< endl;
  cin >> y_c;
  cout << "Wprowadzone dane wygladaja nastepujaco:"<< endl;
  cout << "A("<< x_a <<","<< y_a <<")"<< endl;
  cout << "B("<< x_b <<","<< y_b <<")"<< endl;
  cout << "C("<< x_c <<","<< y_c <<")"<< endl;

  bok_a_b=sqrt(pow(x_b-x_a,2)+pow(y_b-y_a,2));
  bok_b_c=sqrt(pow(x_c-x_b,2)+pow(y_c-y_b,2));
  bok_c_a=sqrt(pow(x_a-x_c,2)+pow(y_a-y_c,2));

  cout << "Dlugosc boku A wynosi: "<< bok_a_b << endl;
  cout << "Dlugosc boku B wynosi: "<< bok_b_c << endl;
  cout << "Dlugosc boku C wynosi: "<< bok_c_a << endl;

  warunek1=(bok_a_b<(bok_b_c+bok_c_a));
  warunek2=(bok_b_c<(bok_a_b+bok_c_a));
  warunek3=(bok_c_a<(bok_a_b+bok_b_c));

  if(warunek1 && warunek2 && warunek3)
  {
    obwod=bok_a_b+bok_b_c+bok_c_a;
    cout << "Obwod trojkata ABC wynosi: "<< obwod << endl;

    pole=sqrt((obwod/2)*((obwod/2)-bok_a_b)*((obwod/2)-bok_b_c)*((obwod/2)-bok_c_a));
    cout << "Pole trojkata ABC wynosi: "<< pole << endl;

    srodek_ciezkosci_x=(x_a+x_b+x_c)/3;
    srodek_ciezkosci_y=(y_a+y_b+y_c)/3;
    cout << "Wspolrzedne srodka ciezkosci trojkata ABC to ("<< srodek_ciezkosci_x <<","<< srodek_ciezkosci_y <<")"<< endl;

    promien_okregu_wpisanego=sqrt(((obwod/2)-bok_a_b)*((obwod/2)-bok_b_c)*((obwod/2)-bok_c_a)/(obwod/2));
    cout << "Dlugosc promienia okregu wpisanego w trojkat ABC wynosi: "<< promien_okregu_wpisanego << endl;

    promien_okregu_opisanego=bok_a_b*bok_b_c*bok_c_a/(4*promien_okregu_wpisanego*(obwod/2));
    cout << "Dlugosc promienia okregu opisanego na trojkacie ABC wynosi: "<< promien_okregu_opisanego << endl;
  }
  else
    cout << "Niestety, korzystajac z tych punktow nie mozna zbudowac trojkata :(" << endl;

  return 0;
}