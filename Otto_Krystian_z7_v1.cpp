#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;
const int w=5, k=6;

void losujZnakiWTablicy(char tab[w][k], char znak_od, char znak_do) {
  srand(time(0));
  for (int i = 0; i < w; i++) {
      for (int j = 0; j < k; j++) {
          tab[i][j] = znak_od + rand( )%(znak_do+1-znak_od);;
      }
  }
};

void drukujTablice(char tab[w][k]) {
  for (int i = 0; i < w; i++) { 
    for (int j = 0; j < k; j++) 
      cout << setw(3)<< tab[i][j]; 
    cout << endl; }
};

void szukajZamienWTablicy(char tab[w][k], char znak_szukaj, char znak_wypelnij) {
  for (int i = 0; i < w; i++) {
    int licznik=0;
    for (int j = 0; j < k; j++) 
      if(tab[i][j]==znak_szukaj)licznik++;
    if (licznik==0){
      for (int j = 0; j < k; j++)
        tab[i][j]=znak_wypelnij;
    }
  }
};

int main()
{
  char Li[w][k],Cy[w][k];
  losujZnakiWTablicy(Li, 'A', 'Z');
  losujZnakiWTablicy(Cy, '0', '9');

  cout << "Tablica Li:"<< endl;
  drukujTablice(Li);
  cout << "Tablica Cy:"<< endl;
  drukujTablice(Cy);

  szukajZamienWTablicy(Li,'P','%');
  szukajZamienWTablicy(Cy,'5','&');

  cout << "Tablica Li po przeksztalceniu:"<< endl;
  drukujTablice(Li);
  cout << "Tablica Cy po przeksztalceniu:"<< endl;
  drukujTablice(Cy);

  return 0;
}
