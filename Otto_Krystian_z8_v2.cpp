#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;
const int n=6, Max=5;


struct Student
{
  char ini[2];
  double ocena;
};

double przetwarzajStudentow(Student tab[n], double &srednia) {
  double najw_ocena=0;
  double sum=0;
  cout << "Tablica rekordow:"<< endl;
  for (int i = 0; i < n; i++){ 
    tab[i].ini[0] ='A' + rand( )%('Z'+1-'A'); 
    tab[i].ini[1] ='A' + rand( )%('Z'+1-'A'); 
    tab[i].ocena = Max*rand( )/double(RAND_MAX);
    cout << fixed << setprecision(2) << setw(6) << tab[i].ini[0] << tab[i].ini[1] << " " << tab[i].ocena;
    sum+=tab[i].ocena;
    if (tab[i].ocena>najw_ocena)najw_ocena=tab[i].ocena;
  }
  cout << endl;
  srednia=sum/n;
  return najw_ocena;
};



int main()
{
  Student G1[n],G2[n];
  double srednia1, srednia2, ocena1, ocena2;

  srand(time(0));

  ocena1=przetwarzajStudentow(G1,srednia1);
  ocena2=przetwarzajStudentow(G2,srednia2);

  cout << "Roznica pomiedzy najwyzsza ocena w G1 a G2 wynosi: " << ocena1-ocena2 << endl;

  if (srednia1>srednia2)
    cout << "G1 ma wyzsza srednia od G2. Srednia ta wynosi: " << srednia1 << endl;
  else
    cout << "G2 ma wyzsza srednia od G1. Srednia ta wynosi: " << srednia2 << endl;
  
  return 0;
}
