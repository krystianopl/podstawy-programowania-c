#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
  const int w=3, k=4, sz=6;
  int high_col_index;
  double a[w][k], avg[k]= {},high_col_value=(-1),tmp;
  srand (time(0));

  cout << "Tablica liczb rzeczywistych przedstawiona z precyzja 1"<< endl;
  for (int i = 0; i < w; i++){ 
    for (int j = 0; j < k; j++){
      a[i][j]=5*rand( )/double(RAND_MAX);
      cout << fixed << setprecision(1) << setw(sz) << a[i][j];
      avg[j]+=a[i][j];
      } 
    cout << endl;
    }

  cout << "Srednia z kolumn powyzszej tablicy przedstawiona z precyzja 2"<< endl;
  for (int j = 0; j < k; j++){
    cout << fixed << setprecision(2) << setw(sz) << avg[j]/w;
    if (avg[j]>high_col_value){
      high_col_value=avg[j];
      high_col_index=j;}
    }
  cout << endl;
  
  for (int i = 0; i < w; i++){ 
    for (int j = 0; j < k; j++){
      if(j==high_col_index)
        tmp=a[i][j];
      if(j==(k-1)){
        a[i][high_col_index]=a[i][j];
        a[i][j]=tmp;}
      } 
    }

  cout << "Pierwotna tablica z przestawiona kolumna o najwyzszej sredniej"<< endl;
  for (int i = 0; i < w; i++){ 
    for (int j = 0; j < k; j++){
      cout << fixed << setprecision(1) << setw(sz) << a[i][j];
      } 
    cout << endl;
    }

  return 0;
}
